<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<%-- This is jsp comment--%>
<!-- This is html comment-->
<body>
    <h2>Java Demo Web Application!</h2>
<img src="static/images/java.jpg" alt="java logo" width="289" height="174">
<hr>
<a href="${pageContext.request.contextPath}/register">Register</a>

<c:if test="${sessionScope.isAuthenticated}">
    <br>
    <a href="${pageContext.request.contextPath}/secured/resource"> Go to secured resource</a>
</c:if>
</body>
</html>
