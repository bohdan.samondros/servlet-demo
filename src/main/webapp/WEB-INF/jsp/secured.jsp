<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="ISO-8859-1">
    <title>JSP Actions Example</title>
</head>
<body>

<h1>Secured Resource</h1>
<br>
<a href="${pageContext.request.contextPath}/secured/download/file.txt">Download file</a>
<br>
<a href="${pageContext.request.contextPath}/secured/upload">Upload file</a>
<br>
<a href="${pageContext.request.contextPath}">Home</a>

</body>
</html>