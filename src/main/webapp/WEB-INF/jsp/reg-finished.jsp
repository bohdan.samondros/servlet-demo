<jsp:useBean id="firstName" scope="request" type="java.lang.String"/>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Registration finished</title>
</head>
<body>
<h1>Congratulations, ${firstName}!</h1>
<p>Registration has successfully finished, <%=hello()%></p>

<a href="${pageContext.request.contextPath}/secured/resource">Go to secured resource</a>
</body>
</html>

<%! String hello() {
    return "hello";
}%>
