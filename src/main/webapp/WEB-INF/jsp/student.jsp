<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="ISO-8859-1">
    <title>JSP Actions Example</title>
</head>
<body>

<h1> Student Registration Page</h1>

<% if (request.getAttribute("errors") != null) { %>
    <h4>Form has errors</h4>
<% } %>
<form action="${pageContext.request.contextPath}/register" method="post">
    <label for="firstName">First Name: </label>
    <input id="firstName" type="text" name="firstName">
    <br> <br>

    <label for="lastName">Last Name: </label>
    <input id="lastName" type="text" name="lastName">
    <br> <br>

    <label for="email">Email: </label>
    <input id="email" type="email" name="emailId">
    <br> <br>

    <label for="password">Password: </label>
    <input id="password" type="password" name="password"><br>

    <br>
    <input type="submit" value="register">
</form>
</body>
</html>