package org.example.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionIdListener;

@WebListener
public class WebListenerDemo implements ServletContextAttributeListener,
        ServletContextListener,
        ServletRequestListener,
        ServletRequestAttributeListener,
        HttpSessionIdListener {
    private static final Logger log = LoggerFactory.getLogger(WebListenerDemo.class);

    /**
     * ServletContextListener
     */
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        log.debug("Context initialized, {}", sce);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        log.debug("Context destroyed, {}", sce);
    }

    /**
     * ServletContextAttributeListener
     */
    @Override
    public void attributeAdded(ServletContextAttributeEvent event) {
        log.debug("Servlet attribute added, {}={}", event.getName(), event.getValue());
    }

    @Override
    public void attributeRemoved(ServletContextAttributeEvent event) {
        log.debug("Servlet attribute removed, {}", event);
    }

    @Override
    public void attributeReplaced(ServletContextAttributeEvent event) {
        log.debug("Servlet attribute replaced, {}", event);
    }

    /**
     * ServletRequestListener
     */

    @Override
    public void requestDestroyed(ServletRequestEvent sre) {
        log.debug("Request destroyed, {}", ((HttpServletRequest) sre.getServletRequest()).getRequestURL());
    }

    @Override
    public void requestInitialized(ServletRequestEvent sre) {
        log.debug("Request initialized, {}", ((HttpServletRequest) sre.getServletRequest()).getRequestURL());
    }

    /**
     * ServletRequestListener
     */

    @Override
    public void attributeAdded(ServletRequestAttributeEvent srae) {
        log.debug("Request attribute added, {}={}", srae.getName(), srae.getValue());
    }

    @Override
    public void attributeRemoved(ServletRequestAttributeEvent srae) {
        log.debug("Request attribute removed, {}", srae);
    }

    @Override
    public void attributeReplaced(ServletRequestAttributeEvent srae) {

    }

    /**
     * HttpSessionIdListener
     */

    @Override
    public void sessionIdChanged(HttpSessionEvent event, String oldSessionId) {

    }
}
