package org.example.servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;
import java.nio.file.Paths;

@WebServlet(urlPatterns = {"/secured/upload"})
@MultipartConfig
public class FileUploadServlet extends HttpServlet {
    private static final Logger log = LoggerFactory.getLogger(FileUploadServlet.class);

    String filePath;

    @Override
    public void init() throws ServletException {
        filePath = getServletContext().getRealPath("WEB-INF/upload/");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/jsp/upload.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String fileName = null;
        for (Part part : req.getParts()) {
            fileName = getFileName(part);
            part.write(filePath + File.separator + fileName);
        }

        req.setAttribute("message", fileName + " File uploaded successfully!");
        getServletContext().getRequestDispatcher("/WEB-INF/jsp/response.jsp").forward(
                req, resp);
    }

    private String getFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        log.info("content-disposition header= "+contentDisp);
        String[] tokens = contentDisp.split(";");
        for (String token : tokens) {
            if (token.trim().startsWith("filename")) {
                return token.substring(token.indexOf("=") + 2, token.length()-1);
            }
        }
        return "";
    }
}
