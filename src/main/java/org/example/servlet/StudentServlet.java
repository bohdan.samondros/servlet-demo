package org.example.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "Student Registration Servlet", urlPatterns = {"/register"})
public class StudentServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String data = "Object arrived from Servlet";

        req.setAttribute("data", data);

        req.getRequestDispatcher("/WEB-INF/jsp/student.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String firstName = req.getParameter("firstName");
        String lastName = req.getParameter("lastName");
        String emailId = req.getParameter("emailId");
        String password = req.getParameter("password");

        // do validation
        boolean hasErrors = false;
        if (password.isBlank()) {
            hasErrors = true;
            req.setAttribute("errors", true);
            req.getRequestDispatcher("/WEB-INF/jsp/student.jsp").forward(req, resp);
        } else {
            // save student
            req.getSession().setAttribute("isAuthenticated", true);
            req.getSession().setAttribute("user", emailId);
        }

        req.setAttribute("firstName", firstName);
        req.getRequestDispatcher("/WEB-INF/jsp/reg-finished.jsp").forward(req, resp);
    }
}
