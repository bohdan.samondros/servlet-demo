package org.example.servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

@WebServlet(name = "HomePageServlet2", urlPatterns = {"/home2"})
public class HomeServlet2 implements Servlet {
    private static final Logger log = LoggerFactory.getLogger(HomeServlet2.class);

    private ServletConfig config = null;

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        config = servletConfig;
        log.debug("Servlet initialization");
    }

    @Override
    public ServletConfig getServletConfig() {
        return this.config;
    }

    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
        log.info("Content-Type: " + servletRequest.getContentType());
        servletRequest.getParameterMap().forEach((key, value) -> log.info(key + ": " + Arrays.toString(value)));

        servletResponse.setContentType("text/html");

        PrintWriter out = servletResponse.getWriter();
        out.print("<html><body>");
        out.print("<b>Home Page Servlet</b>");
        out.print("</body></html>");
    }

    @Override
    public String getServletInfo() {
        return "Some useful info";
    }

    @Override
    public void destroy() {
        log.debug("Servlet destroy. Releasing resources");
    }
}
