package org.example.filter;

import org.example.listener.WebListenerDemo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(filterName = "MyFilter", urlPatterns = {"/secured/*"})
public class WebFilterDemo extends HttpFilter {
    private static final Logger log = LoggerFactory.getLogger(WebFilterDemo.class);

    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpSession session = req.getSession();
        boolean isAuthenticated = Boolean.TRUE == session.getAttribute("isAuthenticated");
        String userId = (String) session.getAttribute("user");

        if (isAuthenticated) {
            log.info("User {} has accessed secured resource {}", userId, req.getRequestURL());
            chain.doFilter(req, res);
        } else {
            res.sendError(401, "Unauthorized");
        }
        log.info("Response from servlet: {}", res.getStatus());
    }
}
